//
//  AutomatorProgress.h
//  AutomatorProgress
//
//  Created by Akasaka Ryuunosuke on 24/07/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import <Automator/AMBundleAction.h>

@interface AutomatorProgress : AMBundleAction

- (id)runWithInput:(id)input fromAction:(AMAction *)anAction error:(NSDictionary **)errorInfo;
@property (strong) IBOutlet NSPopUpButton *option;
@property (strong) IBOutlet NSPopUpButton *updWith;
@property (strong) IBOutlet NSTextField *infProgress;
@property (strong) IBOutlet NSSlider *valProgress;
@property (strong) IBOutlet NSTextField *titleBox;
@property (strong) IBOutlet NSTextField *msgBox;

@end
