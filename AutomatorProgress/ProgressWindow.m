//
//  ProgressWindow.m
//  AutomatorProgress
//
//  Created by Akasaka Ryuunosuke on 24/07/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import "ProgressWindow.h"

@interface ProgressWindow ()
@property (strong) IBOutlet NSTextField *titlebar;
@property (strong) IBOutlet NSProgressIndicator *progressbar;
@end

@implementation ProgressWindow
+ (ProgressWindow *)sharedInstance
{
    static ProgressWindow *sharedInstance = nil;
    if (sharedInstance == nil)
    {
        sharedInstance = [[self alloc] initWithWindowNibName:@"ProgressWindow"];
    }
    return sharedInstance;
}
- (void) updateWindow {
    if(!self.window)return;
    
    [self.progressbar setIndeterminate:self.indeterminate];
    [self.progressbar setDoubleValue:self.progress];
    [self.progressbar startAnimation:self];
    [self.titlebar setStringValue:self.subtitle?:@""];
    [self.window setTitle:self.title?:@""];
}
- (void)windowDidLoad {
    [super windowDidLoad];
    
    
    [self updateWindow];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
}
- (void) center {
    CGFloat xPos = NSWidth([[self.window screen] frame])/2 - NSWidth([self.window frame])/2;
    CGFloat yPos = NSHeight([[self.window screen] frame])/2 - NSHeight([self.window frame])/2;
    [self.window setFrame:NSMakeRect(xPos, yPos, NSWidth([self.window frame]), NSHeight([self.window frame])) display:YES];
    [self updateWindow];
}
@end
