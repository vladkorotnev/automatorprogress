//
//  ProgressWindow.h
//  AutomatorProgress
//
//  Created by Akasaka Ryuunosuke on 24/07/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ProgressWindow : NSWindowController
@property (strong) NSString* title;
@property (strong) NSString* subtitle;
@property NSUInteger progress;
@property bool indeterminate;

+ (ProgressWindow *)sharedInstance;
- (void) center;
- (void) updateWindow;
@end
