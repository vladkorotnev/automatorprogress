//
//  AutomatorProgress.m
//  AutomatorProgress
//
//  Created by Akasaka Ryuunosuke on 24/07/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import "AutomatorProgress.h"
#import "ProgressWindow.h"
@implementation AutomatorProgress
{
    bool windowShown;
}

- (void) activated {
    [super activated];
     [self _optChange:self.option];
}

- (id)runWithInput:(id)input fromAction:(AMAction *)anAction error:(NSDictionary **)errorInfo
{
	// Add your code here, returning the data to be passed to the next action.
    [self performSelectorInBackground:@selector(_showWindow:) withObject:anAction];
	return input;
}
- (IBAction)_optChange:(NSPopUpButton*)sender {

        self.updWith.hidden = !(sender.selectedTag == 2);

    bool showTitleAndMsg = (sender.selectedTag == 0 || (sender.selectedTag == 2 && self.updWith.selectedTag == 2 ));
        self.titleBox.hidden = !showTitleAndMsg;
        self.msgBox.hidden = !showTitleAndMsg;
    
    bool showVal = (sender.selectedTag == 2) && (self.updWith.selectedTag ==1);
    self.valProgress.hidden = !showVal;
    self.infProgress.hidden = !showVal;
 
}
- (IBAction)subOptChange:(NSPopUpButton*)sender {
    
    [self _optChange:self.option];
}
- (IBAction)valChange:(id)sender {
    [self.infProgress setStringValue:[NSString stringWithFormat:@"%li%%", self.valProgress.integerValue]];
}

- (void) parametersUpdated {
    [self _optChange:self.option];
}
- (void) updateParameters {
    [self _optChange:self.option];
}
- (void) opened {
    [self _optChange:self.option];
}
- (void) _showWindow:(AMAction*)action {
    ProgressWindow *w = [ProgressWindow sharedInstance];
    switch ([self.parameters[@"option"] integerValue]) {
        case 0:
            // Show
            [w setSubtitle:self.parameters[@"subtitle"]?:@"Workflow in progress..."];
            [w setTitle:self.parameters[@"title"]?:@"Please wait"];
            [w setIndeterminate:true];
            [w showWindow:self];
            [w center];
            break;
        case 1:
            // hide
            [w.window orderOut:self];
            break;
        case 2:
            // update
            switch ([self.parameters[@"suboption"] integerValue]) {
                case 0:
                    // make indtm
                    [w setIndeterminate:true];
                    break;
                case 1:
                    // val
                    [w setIndeterminate:false];
                    [w setProgress:[self.parameters[@"value"] unsignedIntegerValue]];
                    break;
                case 2:
                    // title
                    [w setSubtitle:self.parameters[@"subtitle"]?:@"Workflow in progress..."];
                    [w setTitle:self.parameters[@"title"]?:@"Please wait"];
                    break;
                    
                default:
                     NSLog(@"WTF in Progress Action: unkn sub-option!");
                    break;
            }
            break;
            
        default:
            NSLog(@"WTF in Progress Action: unkn option!");
            break;
    }
    [w updateWindow];
}

- (void) closed {
    [[ProgressWindow sharedInstance].window orderOut:self];
    [super closed];
}

@end
